<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MuridController extends Controller
{
    public function index()
    {
    	$data_murid = \App\Murid::all();
    	return view('murid.index', ['data_murid' => $data_murid]);
    }
    public function create(Request $request)
    {
    	\App\Murid::create($request->all());
    	return redirect('/murid')->with('sukses','data berhasil diinput');
    }
    public function edit($id)
    {
    	$murid = \App\Murid::find($id);
    	return view('murid/edit',['murid' => $murid]);
    } 	
     public function update(Request $request,$id)
    {
    	$murid = \App\Murid::find($id);
    	$murid ->update($request->all());
    	return redirect('/murid')->with('sukses','data berhasil diinput');
    }
    public function delete($id)
    {
    	$murid = \App\Murid::find($id);
    	$murid ->delete($murid);
    	return redirect('/murid');
    }
}
