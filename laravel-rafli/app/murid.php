<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class murid extends Model
{
    protected $table = 'murid';
    protected $fillable = ['nama_lengkap','jenis_kelamin','agama','alamat'];
}
