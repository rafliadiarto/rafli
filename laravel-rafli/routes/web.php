<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/murid','MuridController@index');
Route::post('/murid/create','MuridController@create');
Route::get('/murid/{id}/edit','MuridController@edit');
Route::post('/murid/{id}/update','MuridController@update');
route::get('/murid/{id}/delete','MuridController@delete');