@extends('layout.master')

@section('content')
    		@if(session('sukses'))
	    		<div class="alert alert-success" role="alert">
				  {{session('sukses')}}
				</div>
			@endif
    		<div class="row">
    			<div class="col-6">
    				<h1>Data Murid</h1>
    			</div>
    			<div class="col-6">
    				<button type="button" class="btn btn-outline-success btn-sm float-right" data-toggle="modal" data-target="#exampleModal">
					  Tambah
					</button>
		</div>
</div>
		    
		<table class="table table-hover">
			<tr>
				<th>Nama Lengkap</th>
				<th>Jenis Kelamin</th>
				<th>Agama</th>
				<th>Alamat</th>
				<th>AKSI</th>

			</tr>
		    @foreach($data_murid as $murid)
			<tr>
				<td>{{$murid->nama_lengkap}}</td>
				<td>{{$murid->jenis_kelamin}}</td>
				<td>{{$murid->agama}}</td>
				<td>{{$murid->alamat}}</td>
				<td><a href="/murid/{{$murid->id}}/edit" class="btn btn-success btn-sm">Edit
				</a>
				<a href="/murid/{{$murid->id}}/delete" class="btn-outline-danger btn-sm" onclick="return confirm('Yakin mau dihapus ?')">Delete</a>
			</td>
			</tr>
			@endforeach
		</table>

					<!-- Modal -->
					<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					  <div class="modal-dialog" role="document">
					    <div class="modal-content">
					      <div class="modal-header">
					        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
					        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
					          <span aria-hidden="true">&times;</span>
					        </button>
					      </div>
					      <div class="modal-body">
					        <form action="/murid/create" method="POST">
					        	{{csrf_field()}}
						  <div class="form-group">
						    <label for="exampleInputEmail1">Nama Lengkap</label>
						    <input name="nama_lengkap" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nama Lengkap">
						    </div>

						    <div class="form-group">
						    <label for="exampleFormControlSelect1">Jenis Kelamin</label>
						    <select class="form-control" id="exampleFormControlSelect1" name="jenis_kelamin">
						      <option value="L">Laki-Laki</option>
						      <option value="P">Perempuan</option>
						       </select>
							 </div>

							 <div class="form-group">
						    <label for="exampleInputEmail1">Agama</label>
						    <input  name="agama" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Agama">
						    </div>

						  <div class="form-group">
						    <label for="exampleInputPassword1">Alamat</label>
						     <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="alamat"></textarea>
						  </div>	  
											      
						</div>
					      <div class="modal-footer">
					        <button type="button" class="btn-outline-danger" data-dismiss="modal">Close</button>
					        <button type="submit" class="btn-outline-success">Submit</button>
						</form>
					      </div>
					    </div>
					  </div>
					  @endsection


  


	